import React from 'react';

import Contacts from './contacts/Contacts';
import Header from './layout/Header';
import AddContact from './contacts/AddContact';


import 'bootstrap/dist/css/bootstrap.min.css'
import { Provider } from '../context';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import About from './pages/About';
import NotFound from './pages/NotFound';


const App = () => {
    return (
        <Provider>
            <Router>
                <div className="App">
                <Header branding="Contact Manager" />
                <div className="container">
                <Switch>
                    <Route exact path="/" component={Contacts} />
                    <Route path="/about" component={About} />
                    <Route path="/contact/add" component={AddContact} />
                    <Route  component={NotFound} />
                </Switch>
                </div>
                </div>
            </Router>
        </Provider>
       
    )
} 

export default App;